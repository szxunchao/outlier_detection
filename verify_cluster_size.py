from pyspark import SparkContext, SparkConf
from collections import OrderedDict

conf = SparkConf().setMaster("local[*]").setAppName("KDDCup99") \
      #.set("spark.executor.memory", "2g")
sc = SparkContext(conf=conf)
raw_data = sc.textFile("/home/xchen/experiments/kddcup.data")

# count by all different labels and print them decreasingly
print "Counting all different labels"
labels = raw_data.map(lambda line: line.strip().split(",")[-1])
label_counts = labels.countByValue()
sorted_labels = OrderedDict(sorted(label_counts.items(), key=lambda t: t[1], reverse=True))
for label, count in sorted_labels.items():
    print label, count


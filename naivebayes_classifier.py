import sys
import os
from numpy import array
try:
    from pyspark import SparkContext, SparkConf
    from pyspark.mllib.feature import StandardScaler
    from pyspark.mllib.classification import NaiveBayes, NaiveBayesModel
    from pyspark.mllib.regression import LabeledPoint
    print ("Successfully imported Spark Modules")
except ImportError as e:
    print ("Can not import Spark Modules", e)
    sys.exit(1)

def parse_interaction(line):
    """
    Parses the results from outlier detection and translate the label.
    """
    line_split = line.split(",")  
       
    clean_line_split = [float(line_split[1].lstrip(" u'"))]
    clean_line_split.extend(map(float,line_split[5:-1]))
    temp=line_split[-1].rstrip(".')")
    if temp in ["smurf","neptune","normal"]:
        clean_line_split.append(0)
    else:
        clean_line_split.append(1)
    return clean_line_split    

#def label_data_pair(line):
#    label = line[0]
#    features = line[1]
#    return LabeledPoint(label,Vectors.dense(features))
      
if __name__=="__main__":
    data_file = sys.argv[1]    

#    conf = SparkConf().setMaster("local[*]").setAppName("KDDCup99")
    conf = SparkConf()
    sc = SparkContext(conf=conf)
    # load labeled raw data
    print "Loading labeled training dataset..."

    raw_data = sc.textFile(data_file).map(parse_interaction)
    features = raw_data.map(lambda row: row[0:-1])

    # Standardize features    
    standardizer = StandardScaler()
    model = standardizer.fit(features)
    features_transform = model.transform(features)

        
    # combine label and features        
    label = raw_data.map(lambda row: row[-1])
    transformedData = label.zip(features_transform)
    label_data= transformedData.map(lambda row: LabeledPoint(row[0],row[1]))
    
    total_accuracy =0.0        
    for i in range(5):   
    # split training and test data
        training, test = label_data.randomSplit([0.6, 0.4],i)
        model_bayes = NaiveBayes.train(training, 1.0)
        print ("============Training Finished================")

    # Make prediction and test accuracy.
        predictionAndLabel = test.map(lambda p: (model_bayes.predict(p.features), p.label))
        accuracy = 1.0 * predictionAndLabel.filter(lambda (x, v): x == v).count() / test.count()
        total_accuracy= total_accuracy + accuracy
        print "=========accuracy after "+ str(i) +"equal to"+ str(accuracy)+ "========="

    avg_accuracy= total_accuracy/ float(5)
    print "====================================================="
    print 'Average Accuracy ='+ str(avg_accuracy)       
    print transformedData.take(5)
    print "====================================================="      

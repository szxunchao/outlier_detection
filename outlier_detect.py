#!/usr/bin/env python

# dataset: http://kdd.ics.uci.edu/databases/kddcup99/kddcup99.html

import sys
import os


# Path for spark source folder
#os.environ['SPARK_HOME']="/path/to/spark"

# Append pyspark  to Python Path
#sys.path.append("/path/to/spark/python")

try:
    from pyspark import SparkContext, SparkConf
    from pyspark.mllib.clustering import KMeans
    from pyspark.mllib.feature import StandardScaler
    print ("Successfully imported Spark Modules")
except ImportError as e:
    print ("Can not import Spark Modules", e)
    sys.exit(1)

from collections import OrderedDict
from numpy import array
from math import sqrt

def parse_interaction(line):
    """
    Parses a network data interaction.
    """
    line_split = line.split(",")
    clean_line_split = [line_split[0]]+line_split[4:-1]
    return (line_split[-1], array([float(x) for x in clean_line_split]))


def distance(a, b):
    """
    Calculates the euclidean distance between two numeric RDDs
    """
    return sqrt(
        a.zip(b)
        .map(lambda x: (x[0]-x[1]))
        .map(lambda x: x*x)
        .reduce(lambda a,b: a+b)
        )


def dist_to_centroid(datum, clusters):
    """
    Determines the distance of a point to its cluster centroid
    """
    cluster = clusters.predict(datum)
    centroid = clusters.centers[cluster]
    return sqrt(sum([x**2 for x in (centroid - datum)]))


def clustering_score(data, k):
    """
    Using kmeans|| instead of random for initialization for better results
    """
    clusters = KMeans.train(data, k, maxIterations=10, runs=5, initializationMode="kmeans||")
    result = (k, clusters, data.map(lambda datum: dist_to_centroid(datum, clusters)).mean())
    print "Clustering score for k=%(k)d is %(score)f" % {"k": k, "score": result[2]}
    return result



if __name__ == "__main__":
    
    if (len(sys.argv) != 4):
        print "Wrong Arguments!" 
        sys.exit(1)

    # set up environment
    max_k = int(sys.argv[1])
    score_th = float(sys.argv[2])
    data_file = sys.argv[3]
    
    #max_k = 30
    #score_th = 0.95
    #data_file = "/home/xchen/experiments/kddcup.data_10_percent" 
    
    conf = SparkConf().setMaster("local[*]").setAppName("KDDCup99") \
      #.set("spark.executor.memory", "2g")
    sc = SparkContext(conf=conf)

    # load raw data
    print "Loading RAW data..."
    raw_data = sc.textFile(data_file)  

    # Prepare data for clustering input
    # the data contains non-numeric features, we want to exclude them since k-means works with numeric features. These are the first three and the last column in each data row
    print "Parsing dataset..."
    parsed_data = raw_data.map(parse_interaction)
    parsed_data_values = parsed_data.values().cache()

    # Standardize data
    print "Standardizing data..."
    standardizer = StandardScaler(True, True)
    standardizer_model = standardizer.fit(parsed_data_values)
    standardized_data_values = standardizer_model.transform(parsed_data_values)

    # Evaluate values of k from 10 to max_k
    print "Calculating total in within cluster distance for different k values (10 to %(max_k)d):" % {"max_k": max_k}
    scores = map(lambda k: clustering_score(standardized_data_values, k), range(10,max_k+1,10))

    # Obtain min score k
    min_k = min(scores, key=lambda x: x[2])[0]
    print "Best k value is %(best_k)d" % {"best_k": min_k}

    # Use the best model to assign a cluster to each datum
    # We use standardized data again here
    print "Obtaining clustering result sample for k=%(min_k)d..." % {"min_k": min_k}
    best_model = min(scores, key=lambda x: x[2])[1]

    # Calculate the size of each cluster
    cluster_number= standardized_data_values.map(lambda datum: str(best_model.predict(datum)))
    #cluster_number_w_rawdata = cluster_number.zip(parsed_data_values)
    cluster_number_w_unparsed_data = cluster_number.zip(raw_data)
    cluster_pair = cluster_number.map(lambda number: (number, 1)).reduceByKey(lambda a, b: a + b).collect()
    sorted_cluster_pair = sorted(cluster_pair, key= lambda t: t[1])
    print "Sizes of each cluster are..."    
    print sorted_cluster_pair

    # Calculate the outlier confidence score for each cluster
    n_max = sorted_cluster_pair[-1][1]
    n_min = sorted_cluster_pair[0][1]

    anomaly_confidence=[]
    for i in sorted_cluster_pair:
        temp= list(i)
        temp[1]= float(n_max-temp[1])/float(n_max-n_min)
        i = tuple(temp)
        anomaly_confidence.append(i)
    print "Anomaly confidence scores associated with each cluster are..."
    print anomaly_confidence    

    # Filter out anomaly point based threshold value    
    anomaly_cluster=[]
    for i in anomaly_confidence:
        if i[1] > score_th:
            anomaly_cluster.append(i[0])
    print "Anomaly clusters are..."
    print anomaly_cluster

    results = cluster_number_w_unparsed_data.filter(lambda x: x[0] in anomaly_cluster)            

    # count by all different labels and print them decreasingly
    print "Counting all different labels..."
    labels = raw_data.map(lambda line: line.strip().split(",")[-1])
    label_counts = labels.countByValue()
    sorted_labels = OrderedDict(sorted(label_counts.items(), key=lambda t: t[1], reverse=True))
    for label, count in sorted_labels.items():
        print label, count
        
    # Save points in anomaly clusters to file
    print "Saving outliers to file..."
    # cluster_assignments_sample = standardized_data_values.map(lambda datum: str(best_model.predict(datum))+","+",".join(map(str,datum))).sample(False,0.05)
    print type(results)
    results.saveAsTextFile("Outliers_To_Be_Verified")
    print "DONE!"


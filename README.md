# Cybersecurity Anomaly Detection System #
This cybersecurity detection system running on Spark consists of three parts:

1. A kMeans clustering based anomaly detection system which determines the best k first, then uses this model and confident scores to detect suspicious activities. The confident score of each cluster is calculated as (N_max - N_x)/(N_max - N_min), where N_max and N_min are the size of largest and smallest cluster, N_x represents the size of a certain cluster.

2. An analyst feedback mechanism which labels anomalies with low confidence score. 

3. A supervised ML model trained by labeled data and help to reduce false positive from Step 1.

================================================================================================
# How To Run #
run autorun\_p1.sh for step1, three arguments are the Max_k in kMeans, confident score threshold, input data path.

run merge_outlier.sh to merge the output from Step 1 into .cvs file

run autorun\_p2.sh to train a naive bayes classifier